import styles from "./Styles/InvoiceItemStyle"
import { Colors } from "../Themes"
import RNText from "../Components/RNText"
import React, { Component } from "react"
import { Text, View, StyleSheet, CheckBox } from "react-native"

export default class RedeemRequestList extends Component {
  state = {
    show: true,
    isSelected: false,
    totalAmount : 0,
    main_id : 0,
    listData : [],
  }  
  onClickCheckBox = (id, data) => {

    this.setState({
      main_id : id 
    })

  //   // console.log("checkbox is clicked",this.state.totalAmount)
  //   var temp = this.state.id

  //   if(temp.indexOf(id) !== -1){
  //     temp.splice(temp.indexOf(id),1)
  //     // var tempData = []

      
      
  // }
  //   else{
  //     temp.push(id)
  //   }
    this.setState(
      {
        isSelected: !this.state.isSelected,
      },
      () => {
        console.log("first step :",this.state.main_id)
        
          this.props.performRedeemAction(this.state.main_id)
        
      }
      


      // () => {
      //   if (this.state.isSelected) {
      //     this.setState({
      //       totalAmount: this.state.totalAmount + (this.props.item.rate * this.props.item.price) * this.props.item.points
      //     })
      //     console.log("when select the list ",this.state.totalAmount)
      //     this.props.performRedeemAction(this.performAction())

      //   } else {
      //     this.setState({
      //       totalAmount: this.state.totalAmount - (this.props.item.rate * this.props.item.price) * this.props.item.points

      //     })
      //     console.log("When remove from the list",this.state.totalAmount)
      //     this.props.performRedeemAction(this.performAction())

      //   }
      // },
    )
  }
  performAction = () => {
    console.log("the total amount is ",this.state.totalAmount)
    return (this.state.totalAmount)
  }



  componentDidMount(){
    this.setState({
      listData : this.props.item
    })
  }
  
  render() {
    const { show } = this.state

    const {id, isSelected,points, rate, amount,totalAmount,} = this.state
    const { item } = this.props

    // const { id, isSelected, points, rate,amount,invoiceRef,date , totalAmount} = this.state
   
    return (
      <View
        style={{
          width: 330,
          paddingLeft: 5,
          borderColor: "#FED340",
          borderWidth: 2,
          top: 10,
          height: 210,
          alignItems: "center",
        }}
      >
        <View
          style={{
            width: "95%",
            justifyContent: "space-between",
            flexDirection: "row",
            height: 30,
          }}
        >
          <RNText style={{ fontWeight: "500" }} tx={`vanSaleScreen.ID`} />
          <Text style={{ fontWeight: "500" }}>{item.id}</Text>
        </View>
        
        <View
          style={{
            width: "95%",
            justifyContent: "space-between",
            flexDirection: "row",
            height: 30,
          }}
        >
          <RNText style={{ fontWeight: "500" }} tx={`vanSaleScreen.Select`} />
          <CheckBox value={this.state.isSelected} onChange={() => this.onClickCheckBox(item.id, item)} />
        </View>

        <View
          style={{
            width: "95%",
            justifyContent: "space-between",
            flexDirection: "row",
            height: 30,
          }}
        >
          <RNText style={{ fontWeight: "500" }} tx={`vanSaleScreen.Points`} />
          <Text style={{ fontWeight: "500" }}>{item.points}</Text>
        </View>

        <View
          style={{
            width: "95%",
            justifyContent: "space-between",
            flexDirection: "row",
            height: 30,
          }}
             

        >
          <RNText style={{ fontWeight: "500" }} tx={`vanSaleScreen.Rate`} />
          <Text style={{ fontWeight: "500" }}>{item.rate}</Text>
        </View>

        <View
          style={{
            width: "95%",
            justifyContent: "space-between",
            flexDirection: "row",
            height: 30,
          }}
        >
          <RNText style={{ fontWeight: "500" }} tx={`vanSaleScreen.Amount`} />
          <Text style={{ fontWeight: "500" }}>{item.amount}</Text>
        </View>
 {/* id: 1
points: 10
rate: "2.00"
amount: "20.00"
redeemed: "No"
invoice_reference: "INV0000000001"
created_at: "2020-03-01T21:10:00.000000Z" */}
        <View
          style={{
            width: "95%",
            justifyContent: "space-between",
            flexDirection: "row",
            height: 30,
          }}
        >
          <RNText style={{ fontWeight: "500" }} tx={`vanSaleScreen.InvoiceRef`} />
        <Text style={{ fontWeight: "500" }}>{item.invoice_reference}</Text>
        </View>

        <View
          style={{
            width: "95%",
            justifyContent: "space-between",
            flexDirection: "row",
            height: 30,
          }}
        >
          <RNText style={{ fontWeight: "500" }} tx={`vanSaleScreen.Date`} />
          <Text style={{ fontWeight: "500" }}>{item.created_at}</Text>
        </View>


      </View>
    )
  }
}

const style = StyleSheet.create({
  Container: {
    marginTop: 20,
    width: "90%",
    alignSelf: "center",
    justifyContent: "space-between",
    flexDirection: "row",
    backgroundColor: "yellow",
  },
  cellContainer: {
    marginTop: 20,
    alignSelf: "center",
    justifyContent: "space-between",
    flexDirection: "row",
  },
})
